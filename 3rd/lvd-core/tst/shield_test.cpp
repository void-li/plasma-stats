/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "core.hpp"  // IWYU pragma: keep

#include <exception>

#include <QObject>
#include <QString>

#include "shield.hpp"

#include "test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

class ShieldTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void allow_void() {
    bool except = false;
                  shield([&] ()            { return;                      },
                         [&] (const QString&) { except = true; });

    QCOMPARE(except, false);
  }

  void allow_quint32() {
    bool except = false;
    auto retval = shield([&] () -> quint32 { return quint32(42         ); },
                         [&] (const QString&) { except = true; return quint32(24         ); });

    QCOMPARE(except, false);
    QCOMPARE(retval, quint32(42         ));
  }

  void allow_qstring() {
    bool except = false;
    auto retval = shield([&] () -> QString { return QString("Mera Luna"); },
                         [&] (const QString&) { except = true; return QString("Luna Mera"); });

    QCOMPARE(except, false);
    QCOMPARE(retval, QString("Mera Luna"));
  }

  // ----------

  void throw_void() {
    bool except = false;
                  shield([&] ()            { LVD_THROW_RUNTIME("Mera Luna"); },
                         [&] (const QString&) { except = true; });

    QCOMPARE(except, true);
  }

  void throw_quint32() {
    bool except = false;
    auto retval = shield([&] () -> quint32 { LVD_THROW_RUNTIME("Mera Luna"); },
                         [&] (const QString&) { except = true; return quint32(); });

    QCOMPARE(except, true);
    QCOMPARE(retval, quint32());
  }

  void throw_qstring() {
    bool except = false;
    auto retval = shield([&] () -> QString { LVD_THROW_RUNTIME("Mera Luna"); },
                         [&] (const QString&) { except = true; return QString(); });

    QCOMPARE(except, true);
    QCOMPARE(retval, QString());
  }

  // ----------

 private: class Exception {};
 private slots:

  void throw_unknown() {
    bool except = false;
                  shield([&] () -> QString { throw Exception(); },
                         [&] (const QString&) { except = true; return QString(); });

    QCOMPARE(except, true);
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ShieldTest)
#include "shield_test.moc"  // IWYU pragma: keep
