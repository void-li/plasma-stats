/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */

// ----------

function amalgamate(lcolor, rcolor, factor) {
  return Qt.rgba(
    lcolor.r * factor + rcolor.r * (1.0 - factor),
    lcolor.g * factor + rcolor.g * (1.0 - factor),
    lcolor.b * factor + rcolor.b * (1.0 - factor),
    lcolor.a * factor + rcolor.a * (1.0 - factor),
  )
}
