/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */

import QtQuick 2.13

import "Root.js" as JS

// ----------

Item {
  id: base

  implicitHeight: grid.height
  implicitWidth:  grid.width

  property bool horizontal_layout

  property color base_color
  property color busy_color
  property color mids_color
  property color idle_color

  property var cpu_model
  property var mem_model
  property var swp_model

  Grid {
    id: grid

    height: base.horizontal_layout ? parent.height : childrenRect.height
    width: !base.horizontal_layout ? parent.width  : childrenRect.width

    columns: base.horizontal_layout ? 2 : 1
    spacing: 2

    GridView {
      id: grid_view

      height:     base.horizontal_layout ? parent.height    : childrenRect.height
      width:     !base.horizontal_layout ? parent.width     : childrenRect.width

      cellHeight: base.horizontal_layout ? parent.height /2 : cellWidth
      cellWidth: !base.horizontal_layout ? parent.width  /2 : cellHeight

      flow:       base.horizontal_layout ? GridView.TopToBottom : GridView.LeftToRight

      model: base.cpu_model

      delegate: Item {
        height: grid_view.cellHeight
        width:  grid_view.cellWidth

        Indicator {
          anchors.centerIn: parent

          height: grid_view.cellHeight - 2
          width:  grid_view.cellWidth  - 2

          borderline_color: base.base_color
          background_color: usage < 0.5 ? JS.amalgamate(base.mids_color, base.idle_color, 2 * (0.0 + usage))
                                        : JS.amalgamate(base.mids_color, base.busy_color, 2 * (1.0 - usage))

          property double usage: model.Usage
        }
      }
    }

    Grid {
      height: base.horizontal_layout ? parent.height : childrenRect.height
      width: !base.horizontal_layout ? parent.width  : childrenRect.width

      columns: base.horizontal_layout ? 1 : 2
      spacing: 0

      Item {
        height: grid_view.cellHeight
        width:  grid_view.cellWidth

        visible: base.mem_model.mem_alive

        Indicator {
          anchors.centerIn: parent

          height: grid_view.cellHeight - 2
          width:  grid_view.cellWidth  - 2

          borderline_color: base.base_color
          background_color: usage < 0.5 ? JS.amalgamate(base.mids_color, base.idle_color, 2 * (0.0 + usage))
                                        : JS.amalgamate(base.mids_color, base.busy_color, 2 * (1.0 - usage))

          property double usage: base.mem_model.mem_usage
        }
      }

      Item {
        height: grid_view.cellHeight
        width:  grid_view.cellWidth

        visible: base.swp_model.swp_alive

        Indicator {
          anchors.centerIn: parent

          height: grid_view.cellHeight - 2
          width:  grid_view.cellWidth  - 2

          borderline_color: base.base_color
          background_color: usage < 0.5 ? JS.amalgamate(base.mids_color, base.idle_color, 2 * (0.0 + usage))
                                        : JS.amalgamate(base.mids_color, base.busy_color, 2 * (1.0 - usage))

          property double usage: base.swp_model.swp_usage
        }
      }
    }
  }
}
