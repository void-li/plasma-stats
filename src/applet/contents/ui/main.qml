/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */

import QtQuick 2.13
import QtQuick.Layouts 1.13

import org.kde.plasma.core 2.0
import org.kde.plasma.plasmoid 2.0

// ----------

Item {
  id: main

  property bool horizontal_layout: plasmoid.formFactor === Types.Horizontal

  Plasmoid.compactRepresentation: Item {
    Layout.fillHeight: main.horizontal_layout
    Layout.fillWidth: !main.horizontal_layout

    Layout.preferredHeight: childrenRect.height
    Layout.preferredWidth:  childrenRect.width

    Root {
      height: main.horizontal_layout ? parent.height : childrenRect.height
      width: !main.horizontal_layout ? parent.width  : childrenRect.width

      horizontal_layout: main.horizontal_layout

      base_color: theme.        textColor
      busy_color: theme.negativeTextColor
      mids_color: theme. neutralTextColor
      idle_color: theme.positiveTextColor

      cpu_model: plasmoid.nativeInterface.nativeInterface().cpu_model()
      mem_model: plasmoid.nativeInterface.nativeInterface().mem_model()
      swp_model: plasmoid.nativeInterface.nativeInterface().mem_model()
    }
  }
}
