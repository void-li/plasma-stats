/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <optional>

#include <QObject>
#include <QString>

#include "lvd/logger.hpp"

#include "plasma_stats.hpp"

// ----------

namespace lvd::plasma::stats {

class PlasmaStatsMemModel : public QObject {
  Q_OBJECT LVD_LOGGER_LIKE(PlasmaStats)

  Q_PROPERTY(bool  mem_alive READ mem_alive NOTIFY mem_changed)
  Q_PROPERTY(float mem_usage READ mem_usage NOTIFY mem_changed)

  Q_PROPERTY(bool  swp_alive READ swp_alive NOTIFY swp_changed)
  Q_PROPERTY(float swp_usage READ swp_usage NOTIFY swp_changed)

 public:
  PlasmaStatsMemModel(PlasmaStats* parent)
      : QObject(parent),
        parent_(parent) {}

 public:
  bool  mem_alive() const {
    return parent_->mem_.has_value();
  }
  float mem_usage() const {
    if (!parent_->mem_) {
      return 0;
    }
    return parent_->mem_->used_val;
  }

  bool  swp_alive() const {
    return parent_->swp_.has_value();
  }
  float swp_usage() const {
    if (!parent_->swp_) {
      return 0;
    }
    return parent_->swp_->used_val;
  }

 signals:
  void mem_changed();
  void swp_changed();

 private:
  PlasmaStats* parent_ = nullptr;
  friend PlasmaStats;
};

}  // namespace lvd::plasma::stats
