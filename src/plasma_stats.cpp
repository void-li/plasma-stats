/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "plasma_stats.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <algorithm>

#include <QFile>
#include <QIODevice>
#include <QQmlEngine>
#include <QStringList>

#include "config.hpp"

#include "plasma_stats_cpu_model.hpp"
#include "plasma_stats_mem_model.hpp"

// ----------

namespace lvd::plasma::stats {

PlasmaStats::PlasmaStats(QObject* parent)
    : QObject(parent) {
  qtimer_ = new QTimer(this);
  qtimer_->setInterval(1024);

  connect(qtimer_, &QTimer::timeout,
          this, &PlasmaStats::on_timeout);

  cpu_model_ = new CpuModel(this);
  qmlRegisterInterface<PlasmaStatsCpuModel>("PlasmaStatsCpuModel", 1);

  mem_model_ = new MemModel(this);
  qmlRegisterInterface<PlasmaStatsMemModel>("PlasmaStatsMemModel", 1);
}

// ----------

void PlasmaStats::execute() {
  LVD_LOG_T();

  qtimer_->start();
}

// ----------

void PlasmaStats::gather_cpu() {
  LVD_LOG_T();

  QFile qfile(config::Cpu_Stat_Path());
  qfile.open(QFile::ReadOnly);

  if (!qfile.isOpen()) {
    LVD_LOG_W() << "cannot read"
                << qfile.fileName();

    return;
  }

  Cpus cpus;

  const QList<QByteArray> lines = qfile.readAll().split('\n');
  for (const QByteArray& line : lines) {
    if (line.startsWith("cpu")) {
      if (line.startsWith("cpu ")) {
        continue;
      }

      uint64_t free_cpu = 0;
      uint64_t used_cpu = 0;

      QList<QByteArray> data = line.split(' ');

      used_cpu += parse_cpu(data, 0x01, "user");
      used_cpu += parse_cpu(data, 0x02, "nice");
      used_cpu += parse_cpu(data, 0x03, "syst");
      free_cpu += parse_cpu(data, 0x04, "idle");
      free_cpu += parse_cpu(data, 0x05, "wait");
      used_cpu += parse_cpu(data, 0x06, "irq0");
      used_cpu += parse_cpu(data, 0x07, "irq1");
      used_cpu += parse_cpu(data, 0x08, "vrt0");
      used_cpu += parse_cpu(data, 0x09, "vrt1");
      used_cpu += parse_cpu(data, 0x0A, "vrt2");

      Q_ASSERT(!data.isEmpty());

      Cpu cpu(data.first());
      cpu.free_cpu = free_cpu;
      cpu.used_cpu = used_cpu;

      cpus.append(cpu);
    }
  }

  for (Cpu& new_cpu : cpus) {
    auto old_cpu = std::find_if(cpus_.begin(),
                                cpus_.end(),
                                [&] (const Cpu& cpu) {
      return cpu.name == new_cpu.name;
    });

    if (old_cpu != cpus_.end()) {
      float free_cpu_diff = new_cpu.free_cpu - old_cpu->free_cpu;
      Q_ASSERT(free_cpu_diff >= 0);

      float used_cpu_diff = new_cpu.used_cpu - old_cpu->used_cpu;
      Q_ASSERT(used_cpu_diff >= 0);

      float full_cpu_diff = free_cpu_diff + used_cpu_diff;
      Q_ASSERT(full_cpu_diff >  0);

      if (full_cpu_diff > 0) {
        float used_val = used_cpu_diff / full_cpu_diff;
        new_cpu.used_val = std::clamp(used_val, 0.0f, 1.0f);
      }
    }
  }

  cpu_model_->beginResetModel();
  LVD_FINALLY {
    cpu_model_->endResetModel();
  };

  cpus_ = cpus;
}

uint64_t PlasmaStats::parse_cpu(const QList<QByteArray>& cpud,
                                int                      index,
                                const QString&           what) {
  if (cpud.size() > index) {
    bool ok;

    uint64_t data = cpud[index].toULongLong(&ok);

    if (!ok) {
      LVD_LOG_W() << "invalid metric"
                  << what
                  << cpud[index];

      return 0;
    }

    return data;
  }

  LVD_LOG_W() << "missing metric"
              << what;

  return 0;
}

void PlasmaStats::gather_mem() {
  LVD_LOG_T();

  QFile qfile(config::Mem_Stat_Path());
  qfile.open(QFile::ReadOnly);

  if (!qfile.isOpen()) {
    LVD_LOG_W() << "cannot read"
                << qfile.fileName();

    return;
  }

  uint64_t free_mem = 0;
  uint64_t full_mem = 0;

  uint64_t free_swp = 0;
  uint64_t full_swp = 0;

  const QList<QByteArray> lines = qfile.readAll().split('\n');
  for (const QByteArray& line : lines) {
    if      (line.startsWith("MemTotal:"    )) {
      full_mem = parse_mem(line, 1, "full_mem");
    }
    else if (line.startsWith("MemAvailable:")) {
      free_mem = parse_mem(line, 1, "free_mem");
    }
    else if (line.startsWith("SwapTotal:"   )) {
      full_swp = parse_mem(line, 1, "full_swp");
    }
    else if (line.startsWith("SwapFree:"    )) {
      free_swp = parse_mem(line, 1, "free_swp");
    }
  }

  if (full_mem > 0) {
    if (!mem_) {
      mem_.emplace("mem");
    }

    mem_->free_mem = free_mem;
    mem_->full_mem = full_mem;

    float used_val = 1.0f - static_cast<float>(free_mem)
                          / static_cast<float>(full_mem);
    mem_->used_val = std::clamp(used_val, 0.0f, 1.0f);

    emit mem_model_->mem_changed();
  } else {
    if ( mem_) {
      mem_.reset();

      emit mem_model_->mem_changed();
    }
  }

  if (full_swp > 0) {
    if (!swp_) {
      swp_.emplace("swp");
    }

    swp_->free_mem = free_swp;
    swp_->full_mem = full_swp;

    float used_val = 1.0f - static_cast<float>(free_swp)
                          / static_cast<float>(full_swp);
    swp_->used_val = std::clamp(used_val, 0.0f, 1.0f);

    emit mem_model_->swp_changed();
  } else {
    if ( swp_) {
      swp_.reset();

      emit mem_model_->swp_changed();
    }
  }
}

uint64_t PlasmaStats::parse_mem(const QString&           mems,
                                int                      index,
                                const QString&           what) {
  QStringList memd = mems.split(' ', Qt::SkipEmptyParts);

  if (memd.size() > index) {
    bool ok;

    uint64_t data = memd[index].toULongLong(&ok);

    if (!ok) {
      LVD_LOG_W() << "invalid metric"
                  << what
                  << memd[index];

      return 0;
    }

    return data;
  }

  LVD_LOG_W() << "missing metric"
              << what;

  return 0;
}

// ----------

void PlasmaStats::on_timeout() {
  LVD_LOG_T();

  gather_cpu();
  gather_mem();
}

}  // namespace lvd::plasma::stats
