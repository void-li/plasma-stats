/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <cstdint>
#include <optional>

#include <QByteArray>
#include <QList>
#include <QObject>
#include <QString>
#include <QTimer>
#include <QVector>

#include "lvd/logger.hpp"

// ----------

namespace lvd::plasma::stats {

class PlasmaStatsCpu {
 public:
  QString name;

  uint64_t free_cpu = 0;
  uint64_t used_cpu = 0;
  float    used_val = 0;

  PlasmaStatsCpu(const QString& name)
      : name(name) {}};

// ----------

class PlasmaStatsMem {
 public:
  QString name;

  uint64_t free_mem = 0;
  uint64_t full_mem = 0;
  float    used_val = 0;

  PlasmaStatsMem(const QString& name)
      : name(name) {}
};

// ----------

class PlasmaStatsCpuModel;
class PlasmaStatsMemModel;

// ----------

class PlasmaStats : public QObject {
  Q_OBJECT LVD_LOGGER

  using Cpu = PlasmaStatsCpu;
  using Cpus = QVector<Cpu>;

  using Mem = PlasmaStatsMem;

  using CpuModel = PlasmaStatsCpuModel;
  friend CpuModel;
  CpuModel* cpu_model_ = nullptr;

  using MemModel = PlasmaStatsMemModel;
  friend MemModel;
  MemModel* mem_model_ = nullptr;

 public:
  PlasmaStats(QObject* parent = nullptr);

  Q_INVOKABLE PlasmaStatsCpuModel* cpu_model() const {
    return cpu_model_;
  }

  Q_INVOKABLE PlasmaStatsMemModel* mem_model() const {
    return mem_model_;
  }

 public slots:
  void execute();

 private:
  void     gather_cpu();
  uint64_t parse_cpu(const QList<QByteArray>& cpud,
                     int                      index,
                     const QString&           what);

  void     gather_mem();
  uint64_t parse_mem(const QString&           memd,
                     int                      index,
                     const QString&           what);

 private slots:
  void on_timeout();

 private:
  QTimer* qtimer_ = nullptr;

  Cpus cpus_;

  std::optional<Mem> mem_;
  std::optional<Mem> swp_;
};

}  // namespace lvd::plasma::stats
