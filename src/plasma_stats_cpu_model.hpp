/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QAbstractListModel>
#include <QByteArray>
#include <QHash>
#include <QModelIndex>
#include <QObject>
#include <QString>
#include <QVariant>

#include "lvd/core.hpp"
#include "lvd/core_enum.hpp"
#include "lvd/logger.hpp"

#include "plasma_stats.hpp"

// ----------

namespace lvd::plasma::stats {

class PlasmaStatsCpuModel : public QAbstractListModel {
  Q_OBJECT LVD_LOGGER_LIKE(PlasmaStats)

 public:
  LVD_ENUM(Roles,
    Usage = Qt::UserRole + 1
  );

 public:
  PlasmaStatsCpuModel(PlasmaStats* parent)
      : QAbstractListModel(parent),
        parent_(parent) {}

 public:
  QHash<int, QByteArray> roleNames() const override;

  int columnCount(const QModelIndex& index = QModelIndex() ) const override;
  int    rowCount(const QModelIndex& index = QModelIndex() ) const override;

  QVariant   data(const QModelIndex& index,
                  int                role = Qt::DisplayRole) const override;

 private:
  PlasmaStats* parent_ = nullptr;
  friend PlasmaStats;
};

}  // namespace lvd::plasma::stats
