/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "plasma_stats_cpu_model.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include "lvd/shield.hpp"

// ----------

namespace lvd::plasma::stats {

QHash<int, QByteArray> PlasmaStatsCpuModel::roleNames() const {
  return
  LVD_SHIELD;

  QHash<int, QByteArray> roleNames;

  for (auto role  = Roles::__vals_begin__();
            role != Roles::__vals_end__  ();
            role ++) {
    roleNames[*role] = Roles::To_String(*role);
  }

  return roleNames;

  LVD_SHIELD_FUN([] (const QString& message) {
    LVD_LOG_W() << message;
    return QHash<int, QByteArray>();
  });
}

int PlasmaStatsCpuModel::columnCount(const QModelIndex& index) const {
  return
  LVD_SHIELD;

  return index.isValid() ? 0 : parent_->cpus_.size(), 1;

  LVD_SHIELD_FUN([] (const QString& message) {
    LVD_LOG_W() << message;
    return 0;
  });
}

int PlasmaStatsCpuModel::   rowCount(const QModelIndex& index) const {
  return
  LVD_SHIELD;

  return index.isValid() ? 0 : parent_->cpus_.size();

  LVD_SHIELD_FUN([] (const QString& message) {
    LVD_LOG_W() << message;
    return 0;
  });
}

QVariant PlasmaStatsCpuModel::  data(const QModelIndex& index,
                                     int                role) const {
  return
  LVD_SHIELD;

  if (   index.row() >=               0x00
      && index.row() < parent_->cpus_.size()) {
    const PlasmaStatsCpu& cpu = parent_->cpus_[index.row()];

    switch (role) {
      case Roles::Usage:
        return QVariant(cpu.used_val);
    }
  }

  LVD_LOG_W() << "invalid role";
  return QVariant();

  LVD_SHIELD_FUN([] (const QString& message) {
    LVD_LOG_W() << message;
    return QVariant();
  });
}

}  // namespace lvd::plasma::stats
