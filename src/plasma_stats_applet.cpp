/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "plasma_stats_applet.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QMetaObject>
#include <QQmlEngine>

#include <KPluginMetaData>  // IWYU pragma: keep
// IWYU pragma: no_include <kpluginmetadata.h>

// ----------

namespace lvd::plasma::stats {

PlasmaStatsApplet::PlasmaStatsApplet(QObject* parent, const QVariantList& args)
    : Plasma::Applet(parent, KPluginMetaData(), args) {
  lvd::Logger::install();

  // ----------

  plasma_stats_ = new PlasmaStats(this);
  qmlRegisterInterface<PlasmaStats>("PlasmaStats", 1);

  QMetaObject::invokeMethod(plasma_stats_, [&] {
    plasma_stats_->execute();
  }, Qt::QueuedConnection);
}

}  // namespace lvd::plasma::stats

// ----------

K_EXPORT_PLASMA_APPLET_WITH_JSON(plasma_stats_applet,
  lvd::plasma::stats::PlasmaStatsApplet, "metadata.json")

#include "plasma_stats_applet.moc"  // IWYU pragma: keep
