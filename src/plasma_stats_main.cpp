/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <cstdlib>
#include <exception>

#include <QCommandLineParser>
#include <QMetaObject>
#include <QModelIndex>
#include <QObject>
#include <QString>
#include <QVariant>

#include "lvd/application.hpp"
#include "lvd/logger.hpp"
#include "lvd/metatype.hpp"

#include "config.hpp"
#include "plasma_stats.hpp"
#include "plasma_stats_cpu_model.hpp"
#include "plasma_stats_mem_model.hpp"

// ----------

int main(int argc, char* argv[]) {
  lvd::Metatype::metatype();

  lvd::Application::setApplicationName (lvd::plasma::stats::config::App_Name());
  lvd::Application::setApplicationVersion(lvd::plasma::stats::config::App_Vers());

  lvd::Application::setOrganizationName(lvd::plasma::stats::config::Org_Name());
  lvd::Application::setOrganizationDomain(lvd::plasma::stats::config::Org_Addr());

  lvd::Application app(argc, argv);

  lvd::Application::connect(&app, &lvd::Application::failure,
  [] (const QString& message) {
    if (!message.isEmpty()) {
      lvd::qStdErr() << message << Qt::endl;
    }

    lvd::Application::exit(1);
  });

  // ----------

  lvd::Logger::install();
  LVD_LOGFUN

  // ----------

  QCommandLineParser qcommandlineparser;
  qcommandlineparser.   addHelpOption();
  qcommandlineparser.addVersionOption();

  lvd::Logger::setup_arguments(qcommandlineparser);

  qcommandlineparser.process(app);

  lvd::Logger::parse_arguments(qcommandlineparser);
  lvd::Application::print();

  // ----------

  try {
    lvd::plasma::stats::PlasmaStats plasma_stats;

    QMetaObject::invokeMethod(&plasma_stats, [&] {
      plasma_stats.execute();
    }, Qt::QueuedConnection);

    QObject::connect(plasma_stats.cpu_model(),
                     &lvd::plasma::stats::PlasmaStatsCpuModel::modelReset,
                     plasma_stats.cpu_model(),
                     [plasma_stats_cpu_model = plasma_stats.cpu_model()] {
      QString message;

      for (int i = 0; i < plasma_stats_cpu_model->rowCount(); i ++) {
        QModelIndex qmodelindex = plasma_stats_cpu_model->index(i);

        message += "[";
        message += QString().setNum(plasma_stats_cpu_model->data(qmodelindex, lvd::plasma::stats::PlasmaStatsCpuModel::Roles::Usage).toFloat());
        message += "]";

        message += " ";
      }

      message.chop(1);

      LVD_LOG_I() << "cpu" << message;
    });

    QObject::connect(plasma_stats.mem_model(),
                     &lvd::plasma::stats::PlasmaStatsMemModel::mem_changed,
                     plasma_stats.mem_model(),
                     [plasma_stats_mem_model = plasma_stats.mem_model()] {
      LVD_LOG_I() << "mem" << plasma_stats_mem_model->mem_usage();
    });

    QObject::connect(plasma_stats.mem_model(),
                     &lvd::plasma::stats::PlasmaStatsMemModel::swp_changed,
                     plasma_stats.mem_model(),
                     [plasma_stats_swp_model = plasma_stats.mem_model()] {
      LVD_LOG_I() << "swp" << plasma_stats_swp_model->swp_usage();
    });

    return app.exec();
  }
  catch (const std::exception& ex) {
    LVD_LOG_C() << "exception:" << ex.what();
  }
  catch (...) {
    LVD_LOG_C() << "exception!";
  }

  return EXIT_FAILURE;
}
